import 'dart:async';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TOGWorld',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: SplashPage(),
    );
  }
}

class SplashPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    new Future.delayed(new Duration(seconds: 2), () {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> MainPage()));
    });
    return Material(
      child: Center(
        child: Image.asset("images/logo.png", fit: BoxFit.cover),
      )
    );
  }
}

class MainPage extends StatefulWidget {
  @override
  _MainPage createState() => _MainPage();
}

class _MainPage extends State<MainPage> {
  final Completer<WebViewController> _controller = Completer<WebViewController>();
  bool isLoading = true;
  WebView webView;
  Widget _buildLoading(){
    return Container(
        color: Colors.black.withOpacity(0.5),
        child: Center(
            child: CircularProgressIndicator()
        )
    );
  }

  Widget _buildWebView(){
    if(webView == null){
      webView = WebView(
        initialUrl: 'http://m.tog.ng/login',
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) {
//          webViewController.loadUrl('http://m.tog.ng/login');
          _controller.complete(webViewController);
        },
        javascriptChannels: <JavascriptChannel>[
          _toasterJavascriptChannel(context),
        ].toSet(),
        navigationDelegate: (NavigationRequest request) {
          print('allowing navigation to $request');
          return NavigationDecision.navigate;
        },
        onPageStarted: (String url) {
          print('Page started loading: $url');
          setState(() {
            isLoading = true;
          });
        },
        onPageFinished: (String url) {
          print('Page finished loading: $url');
          setState(() {
            isLoading = false;
          });
        },
        gestureNavigationEnabled: true,
      );
    }
    return webView;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: <Widget>[
          Positioned.fill(child: Scaffold(
            resizeToAvoidBottomInset: true,
            body: Builder(builder: (BuildContext context) {
              return _buildWebView();
            })
          )),
          isLoading ? _buildLoading() : Container()
        ]
      ),
    );
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
//          Scaffold.of(context).showSnackBar(
//            SnackBar(content: Text(message.message)),
//          );
        });
  }
}
